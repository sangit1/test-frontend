import { ArticleList } from "@/app/components";
import { PageHeader } from "@/app/components/Utils";
import React from "react";

const Page = async ({ params }) => {
  const { keyword } = params;

  const response = await fetch(
    `https://newsapi.org/v2/everything?domains=techcrunch.com&q=${keyword}&apiKey=3341deda0b2147ef8c8c1702d56241b6`
  );
  const data = await response.json();

  return (
    <div>
      <PageHeader
        title="Page Search"
        centerText={`Search results for "${keyword}"`}
        link="/"
        linkTitle="back"
      />
      <ArticleList data={data.articles} />
    </div>
  );
};

export default Page;
