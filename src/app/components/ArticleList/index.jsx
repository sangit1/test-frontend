import React from "react";
import { PageHeader } from "../Utils";
import { CardArticle } from "..";

const ArticleList = ({ data }) => {
  return (
    <>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4 sm:grid-cols-2 p-4">
        {data?.map((item) => (
          <CardArticle key={item.id} item={item}/>
        ))}
      </div>
    </>
  );
};

export default ArticleList;
