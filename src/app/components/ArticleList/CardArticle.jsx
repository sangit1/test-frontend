"use client";
import React, { useState } from "react";
import ModalComponent from "../Modal";
import moment from "moment";

const CardArticle = ({ item }) => {
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "50%",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };

  const [open, setOpen] = useState(false);

  const openModal = () => {
    setOpen(true);
  };
  return (
    <>
      <article
        className="flex max-w-xl flex-col items-start justify-between shadow-md p-1 cursor-pointer"
        onClick={openModal}
      >
        <div className="flex items-center gap-x-4 text-xs">
          <time dateTime="2020-03-16" className="text-gray-500">
            {moment(item.published_At).format("LLLL")}
          </time>
          <a
            href="#"
            className="relative rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100"
          >
            {item.source.name}
          </a>
        </div>
        <img
          className="mt-3 h-40 w-full rounded-lg object-cover"
          src={item.urlToImage}
          alt={item.title}
        />
        <div className="group relative">
          <h3 className="mt-3 text-lg font-semibold leading-6 text-gray-900 group-hover:text-gray-600">
            <a href="#">
              <span className="absolute inset-0" />
              {item.title}
            </a>
          </h3>
          <p className="mt-5 line-clamp-3 text-sm leading-6 text-gray-600">
            {item.description}
          </p>
        </div>
        <div className="relative mt-8 flex items-center gap-x-4">
          <div className="text-sm leading-6">
            <p className="font-semibold text-gray-900">
              <a href="#">
                <span className="absolute inset-0" />
                {item.author}
              </a>
            </p>
          </div>
        </div>
      </article>
      <ModalComponent
        open={open}
        setOpen={setOpen}
        item={item}
        style={customStyles}
      />
    </>
  );
};

export default CardArticle;
