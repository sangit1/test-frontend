import Link from "next/link";
import { DateRange } from "../Utils";
import InputSearch from "../Utils/inputSearch";
import InputDate from "../Utils/inputDate";

const Navbar = () => {
  return (
    <div className="navbar bg-base-100 p-4">
      <div className="flex-1">
        <Link href={"/"} className="btn btn-ghost text-xl">
          <p>test<span className="text-red-400">Frontend</span> </p>
        </Link>
      </div>
      <div>
        <InputSearch />
      </div>
    </div>
  );
};

export default Navbar;
