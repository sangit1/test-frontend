"use client";
import moment from "moment";
import Link from "next/link";
import React from "react";
import Modal from "react-modal";

const ModalComponent = ({ item, open, setOpen, style }) => {
  return (
    <Modal isOpen={open} style={style} onRequestClose={() => setOpen(false)}>
      <div className="bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
        <img
          className="mt-3 h-40 w-full rounded-lg object-cover"
          src={item.urlToImage}
          alt={item.title}
        />
        <div className="sm:flex sm:items-start">
          <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
            <h3
              className="text-base font-semibold leading-6 text-gray-900"
              id="modal-title"
            >
              {item.title}
              <span className="text-slate-500 text-sm font-normal ml-10">
                by {item.author}
              </span>
              
            </h3>
            <div className="mt-2">
              <small className="text-sm text-gray-500">{ moment(item.publishedAt).format("LLLL") }</small>
            </div>
            <div className="mt-2">
              <p className="text-sm text-gray-500">{item.description}</p>
            </div>
            <div className="mt-2">
              <p className="text-sm text-gray-500">{item.content}</p>
            </div>
            <div className="mt-2">
              <Link href={item.url} className="text-sm text-gray-500" target="_blank">see full article</Link>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
        <button
          onClick={() => setOpen(false)}
          type="button"
          className="btn btn-error btn-sm btn-outline"
        >
          Close
        </button>
      </div>
    </Modal>
  );
};

export default ModalComponent;
