import Link from 'next/link'
import React from 'react'

const BtnLink = ({link,linkTitle}) => {
  return (
    <Link href={link} className='btn btn-ghost btn-sm'>{linkTitle}</Link>
  )
}

export default BtnLink