import Link from 'next/link'
import React from 'react'
import { BtnLink } from '../Button'

const PageHeader = ({title,link,linkTitle,centerText}) => {
  return (
    <div className='px-6 flex justify-between items-center'>
      <div className='text-md text-slate-600 font-semibold'>{title}</div>
      <div>{centerText}</div>
      <BtnLink link={link} linkTitle={linkTitle} />
    </div>
  )
}

export default PageHeader