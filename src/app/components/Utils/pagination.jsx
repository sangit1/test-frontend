import React from 'react'

const Pagination = ({currentPage, totalPages, handleNext, handlePrev}) => {
  return (
    <div className='flex justify-center items-center'>
        <button onClick={handlePrev} className='btn btn-ghost btn-sm'>Prev</button>
        {currentPage} of {totalPages}
        <button onClick={handleNext} className='btn btn-ghost btn-sm'>Next</button>
    </div>
  )
}

export default Pagination