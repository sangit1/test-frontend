"use client";
import moment from "moment";
import React from "react";

const InputDate = ({ startDate, setStartDate, endDate, setEndDate }) => {
  return (
    <>
      <div className="mb-4 flex justify-center items-center ">
        <div>
          <label>Start Date:</label>
          <input
            type="date"
            value={startDate}
            onChange={(e) =>
              setStartDate(moment(e.target.value).format("YYYY-MM-DD"))
            }
          />
        </div>
        <div>
          <label>End Date:</label>
          <input
            type="date"
            value={endDate}
            onChange={(e) =>
              setEndDate(moment(e.target.value).format("YYYY-MM-DD"))
            }
          />
        </div>
      </div>
    </>
  );
};

export default InputDate;
