import Footer from './Footer';
import Navbar from './Navbar';
import Hero from './Hero';
import ArticleList from './ArticleList';
import CardArticle from './ArticleList/CardArticle';
import ModalComponent from './Modal';

export { Footer, Navbar , Hero, ArticleList,CardArticle,ModalComponent}