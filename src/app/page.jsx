import { ArticleList, Hero } from "./components";
import { PageHeader } from "./components/Utils";

const Page = async () => {
  const response = await fetch(`https://newsapi.org/v2/everything?domains=techcrunch.com&pageSize=6&apiKey=3341deda0b2147ef8c8c1702d56241b6`);
  const data = await response.json();
  const title = "Top Headlines";
 
  return (
    <>
      <Hero />
      <PageHeader title={title} centerText=""  link='/result' linkTitle='see all articles'/>
      <ArticleList data={data.articles} />
    </>
  );
};

export default Page;
