"use client";
import React, { useEffect, useState } from "react";
import { ArticleList } from "../components";
import { PageHeader, Pagination } from "../components/Utils";
import InputDate from "../components/Utils/inputDate";

const Page = () => {
  const [articles, setArticles] = useState([]);
  const limit = 6;
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [filteredData, setFilteredData] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  useEffect(() => {
    const getArticles = async () => {
      const response = await fetch(
        `https://newsapi.org/v2/everything?domains=techcrunch.com&pageSize=${limit}&page=${currentPage}&apiKey=3341deda0b2147ef8c8c1702d56241b6`
      );
      const data = await response.json();

      setArticles(data.articles);

      setTotalPages(Math.ceil(data.totalResults / limit));
    };
    getArticles();
  }, [currentPage, totalPages]);

  const filterData = () => {
    if (startDate && endDate) {
      const start = new Date(startDate);
      const end = new Date(endDate);

      const filtered = articles.filter((item) => {
        const itemDate = new Date(item.publishedAt);
        return itemDate >= start && itemDate <= end;
      });

      setFilteredData(filtered);
    } else {
      setFilteredData(articles);
    }
  };

  useEffect(() => {
    filterData();
  }, [startDate, endDate, articles]);

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  const handlePrev = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
    scrollTop();
  };

  const handleNext = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
    scrollTop();
  };
  return (
    <div className="px-6">
      <PageHeader
        title="All Articles"
        centerText={`page : ${currentPage} `}
        link="/"
        linkTitle="back"
      />
      <InputDate
        startDate={startDate}
        endDate={endDate}
        setStartDate={setStartDate}
        setEndDate={setEndDate}
      />
      <ArticleList data={filteredData} />
      <Pagination
        totalPages={totalPages}
        currentPage={currentPage}
        handleNext={handleNext}
        handlePrev={handlePrev}
      />
    </div>
  );
};

export default Page;
