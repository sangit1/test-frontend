# FRONT END ENGINEER TECNICAL TEST BINAR LABS

## Membuat aplikasi portal news sederhana dengan instruksi yang sudah ditentukan

## Features

- Menampilkan berita dari api yang sudah disediakan
- User dapat memfilter berdasarkan range waktu berita di publish
- User dapat mensortir berita dengan mengetikkan keyword di input search
- User dapat melihat detail berita dengan mengklik berita dan dimunculkan dengan popup/modal

## Teknologi yang digunakan 

- NextJs 
- Tailwind CSS (daisy UI plugin component library)
- newsapi.org sebagai penyedia rest api dan kami batasi untuk domain techcrunch 
  base api https://newsapi.org/v2/everything?domains=techcrunch.com

## Setup dan instalasi 

1. Clone dari repository :

- git clone https://gitlab.com/sangit1/test-frontend.git

2. Navigasi menuju ke direktori project 

- cd TEST-FRONTEND

3. Menginstall dependensi 

- npm install 

4. Menjalankan Development Server

- npm run dev 

5. Menjalankan aplikasi 

- menjalankan link http://localhost:3000 untuk melihat aplikasi.




